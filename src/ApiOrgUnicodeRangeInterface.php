<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use Iterator;
use Stringable;

/**
 * ApiOrgUnicodeRangeInterface class file.
 * 
 * This class represents a derived name range for the unicode codepoints.
 * 
 * @author Anastaszor
 * @extends \Iterator<integer, ApiOrgUnicodeNameInterface>
 */
interface ApiOrgUnicodeRangeInterface extends Iterator, Stringable
{
	
	/**
	 * Gets whether this range has all its codepoint before the given codepoint.
	 * 
	 * @param ?string $codepoint
	 * @return boolean
	 */
	public function isBefore(?string $codepoint) : bool;
	
	/**
	 * Gets whether this range has all its codepoints after the given codepoint.
	 * 
	 * @param ?string $codepoint
	 * @return boolean
	 */
	public function isAfter(?string $codepoint) : bool;
	
	/**
	 * Gets whether the given code point is within this block.
	 * 
	 * @param ?string $codepoint the hexa representation of the codepoint
	 * @return boolean true if included, false else
	 */
	public function containsCodepoint(?string $codepoint) : bool;
	
}
