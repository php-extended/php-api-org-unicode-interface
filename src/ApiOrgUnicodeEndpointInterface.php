<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use InvalidArgumentException;
use Iterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Stringable;

/**
 * ApiOrgUnicodeEndpointInterface class file.
 * 
 * This class represents the www.unicode.org website and has all the methods
 * to gather data files to interpret them.
 * 
 * @author Anastaszor
 */
interface ApiOrgUnicodeEndpointInterface extends Stringable
{
	
	/**
	 * Gets all the versions available in the database.
	 * 
	 * @return array<integer, string>
	 * @throws ClientExceptionInterface
	 * @todo parse the versions
	 */
	public function getVersions() : array;
	
	/**
	 * Gets the latest version string that is available in the database.
	 * 
	 * @return string
	 * @throws ClientExceptionInterface
	 */
	public function getLatestVersion() : string;
	
	/**
	 * Gets the blocks from the $version/ucd/Blocks.txt ucd data file.
	 * 
	 * @param string $version, defaults to null, meaning the latest stable version
	 * @return Iterator<ApiOrgUnicodeBlockInterface>
	 * @throws InvalidArgumentException if the version number is invalid
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getBlocks(?string $version = null) : Iterator;
	
	/**
	 * Gets all the codepoints for the given version of the unicode standard.
	 * 
	 * @param string $version, defaults to null, meaning the latest stable version
	 * @return Iterator<ApiOrgUnicodeCodepointInterface>
	 * @throws InvalidArgumentException if the version number is invalid
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCodepoints(?string $version = null) : Iterator;
	
	/**
	 * Gets the derived names from the $version/ucd/extracted/DerivedName.txt
	 * data file.
	 * 
	 * @param string $version, defaults to null, meaning the latest stable version
	 * @return Iterator<ApiOrgUnicodeRangeInterface>
	 * @throws InvalidArgumentException if the verison number is invalid
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDerivedNameRanges(?string $version = null) : Iterator;
	
}
