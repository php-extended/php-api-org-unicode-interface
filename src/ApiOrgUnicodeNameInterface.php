<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use Stringable;

/**
 * ApiOrgUnicodeNameInterface class file.
 * 
 * This class represents a derived name for a given codepoint.
 * 
 * @author Anastaszor
 */
interface ApiOrgUnicodeNameInterface extends Stringable
{
	
	/**
	 * Gets the hexa string of the codepoint value.
	 * 
	 * @return string
	 */
	public function getCodepoint() : string;
	
	/**
	 * Gets the name of the codepoint.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
