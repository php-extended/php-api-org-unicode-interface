<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use Stringable;

/**
 * ApiOrgUnicodeCodepointInterface class file.
 * 
 * This class represents a derived name for a given codepoint.
 * 
 * @author Anastaszor
 */
interface ApiOrgUnicodeCodepointInterface extends Stringable
{
	
	/**
	 * Gets the hexa string of the codepoint value in the form of HHHH value.
	 * 
	 * @return string
	 */
	public function getCodepoint() : string;
	
	/**
	 * Gets the unicode codepoint in the form of 'U+HHHH' value.
	 * 
	 * @return string
	 */
	public function getUnicodeCodepoint() : string;
	
	/**
	 * Gets the codepoint notation for json in the form of \uHHHH value.
	 * 
	 * @return string
	 */
	public function getJsonCodepoint() : string;
	
	/**
	 * Gets the codepoint syntax for php in the form of \u{HHHH} value.
	 * 
	 * @return string
	 */
	public function getPhpCodepoint() : string;
	
	/**
	 * Gets the value of the codepoint on the "\xFF\xFF" syntax.
	 * 
	 * @return string
	 */
	public function getUtf8Chain() : string;
	
	/**
	 * Gets the real stringy value of the codepoint.
	 * For U+6211, it will be '我'.
	 * 
	 * @return string
	 */
	public function getUtf8String() : string;
	
	/**
	 * Gets the name of the codepoint.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the block of the codepoint.
	 * 
	 * @return ApiOrgUnicodeBlockInterface
	 */
	public function getBlock() : ApiOrgUnicodeBlockInterface;
	
}
