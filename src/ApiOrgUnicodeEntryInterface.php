<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use Stringable;

/**
 * ApiOrgUnicodeEntryInterface class file.
 * 
 * This class represents a folder entry when listing available files.
 * 
 * @author Anastaszor
 */
interface ApiOrgUnicodeEntryInterface extends Stringable
{
	
	/**
	 * Gets whether this entry represents a file entry.
	 * 
	 * @return boolean
	 */
	public function isFile() : bool;
	
	/**
	 * Gets whether this entry represents a directory.
	 * 
	 * @return boolean
	 */
	public function isFolder() : bool;
	
	/**
	 * Gets the file type of the entry.
	 * 
	 * @return string
	 */
	public function getFileType() : string;
	
	/**
	 * Gets the path of the entry.
	 * 
	 * @return string
	 */
	public function getPath() : string;
	
	/**
	 * Gets the name of the entry.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
