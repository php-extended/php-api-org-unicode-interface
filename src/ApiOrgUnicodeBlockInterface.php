<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-unicode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiOrgUnicode;

use Stringable;

/**
 * ApiOrgUnicodeBlockInterface class file.
 * 
 * This class represents a block for the unicode codepoints.
 * 
 * @author Anastaszor
 */
interface ApiOrgUnicodeBlockInterface extends Stringable
{
	
	/**
	 * Gets the first codepoint of the block.
	 * 
	 * @return string
	 */
	public function getFirstCodepoint() : string;
	
	/**
	 * Gets the last codepoint of the block.
	 * 
	 * @return string
	 */
	public function getLastCodepoint() : string;
	
	/**
	 * Gets the name of the block.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether the given code point is within this block.
	 * 
	 * @param ?string $codepoint the hexa representation of the codepoint
	 * @return boolean true if included, false else
	 */
	public function containsCodepoint(?string $codepoint) : bool;
	
}
